#!/bin/sh


kubectl get pod -n ic-webapp
kubectl get svc -n ic-webapp
kubectl get deploy -n ic-webapp

kubectl delete deployment --all --namespace=ic-webapp
kubectl delete svc  --all --namespace=ic-webapp
kubectl delete pods  --all --namespace=ic-webapp

kubectl apply -f ./ic-webapp
#kubectl apply -f./home/vagrant/ic-webapp

sleep 15s

kubectl get pod -n ic-webapp
kubectl get svc -n ic-webapp
kubectl get deploy -n ic-webapp
