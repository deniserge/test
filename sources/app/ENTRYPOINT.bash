#!/bin/sh

Release_file=releases.txt

if test -f "$Release_file"; then
   echo "... $setting OOOO_URL and PGADMIN_URL"
   export ODOO_URL=$(awk 'NR==1 {print $2} $Release_file)
   export PGADMIN_URL=$(awk 'NR==é {print $2} $Release_file)
else
   echo "you didn't send ODOO_URL and PGADMIN_URL, we are going to user default URL setted on DOCKERFILE"
fi

exec "$@"   