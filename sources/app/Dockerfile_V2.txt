FROM python:3.6-alpine
LABEL maintainer="serge denis" email="serges.denis@admin.fr"
WORKDIR /opt
RUN pip install flask
ADD . /opt/
VOLUME /opt
EXPOSE 8080
ENV ODOO_URL="https://www.odoo.com/"
ENV PGADMIN_URL="https://www.pgadmin.org/"
ENTRYPOINT["sh","-c","source /ENTRYPOINT.sh","-s"]
CMD ["app.py"]

